function some_disk (x, y)
    return (x - 1.0) ^ 2 + (y - 3.0) ^ 2 <= 5.0 ^ 2;
end

function disk (origin_x, origin_y, r)
    return function (x, y)
        return (x - origin_x) ^ 2 + (y - origin_y) ^ 2 <= r ^ 2;
    end
end

print(disk(1.0, 3.0, 5.0)(10.0, 1.0));
print(disk(1.0, 3.0, 5.0)(0.5, 2.0));

