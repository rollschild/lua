function cond2int (x)
    return math.tointeger(x) or x; -- nil or x
end

print(cond2int(4.6)); -- 4.6
print(cond2int(5.0)); -- 5
print(cond2int(20.37)); -- 20.37
