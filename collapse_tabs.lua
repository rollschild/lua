local expand_tabs_module = require "expand_tabs_module";
local collapse_tabs;
function collapse_tabs (str, tab)
    tab = tab or 2;
    str = expand_tabs_module.expand_tabs(str, tab);
    print("after expansion: ", str);
    local pattern = string.rep(".", tab);
    print("pattern: ", pattern);
    str = string.gsub(str, pattern, "%0\7");
    print(str);
    str = string.gsub(str, " +\7", "\\t");
    print(str);
    str = string.gsub(str, "\7", "");
    return str;
end

print(collapse_tabs("hello    , this is    guangchu       shi."));
