local case_conversion;
function case_conversion (str)
    return (string.gsub(str, "%a", function (word)
        return "[" .. string.lower(word) .. string.upper(word) .. "]";
    end));
end

print(case_conversion("what's up.."));
-- string.gsub() returns a second value: number of replacements made
