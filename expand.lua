function expand (str)
    return (string.gsub(str, "$(%w+)", _G));
end

name = "Kobe Bryant";
profession = "basketball";

print(expand("$name is the greatest $profession player of all time"));

function expand_general (str)
    return (string.gsub(str, "$(%w+)", function (n)
        return tostring(_G[n]);
    end));
end

print(expand_general("$name is the greatest $profession player of all time"));
