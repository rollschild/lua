-- state machine
goto room1;

::room1::
do
    local direction = io.read();
    if direction == "south" then 
        goto room3;
    elseif direction == "east" then
        goto room2;
    else 
        io.write("Invalid move!\n");
        goto room1;
    end
end

::room2::
do
    local direction = io.read();
    if direction == "west" then 
        goto room1;
    elseif direction == "south" then
        goto room4;
    else 
        io.write("Invalid move!\n");
        goto room2;
    end
end

::room3::
do
    local direction = io.read();
    if direction == "north" then 
        goto room1;
    elseif direction == "east" then
        goto room4;
    else 
        io.write("Invalid move!\n");
        goto room3;
    end
end

::room4::
do
    io.write("Congratulations! You've reached the destination!\n");
end
