local f = io.popen("ls", "r");
local t = {};
for dir in f:lines() do
    t[#t + 1] = dir;
end

-- print the table
for _, dir in ipairs(t) do
    io.write(string.format("%s\n", dir));
end

f:close();
