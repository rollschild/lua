local date = "today is 05/21/2019";
local d, m, y = string.match(date, "(%d+)/(%d+)/(%d+)");
print(d, m, y);

local words = [[then he said... "We're here chilin'"]];
local q, quoted = string.match(words, "([\"'])(.-)%1");
print(quoted);
print(q);

local pattern = "%[(=*)%[(.-)%]%1%]";
local really_long_string = "[===[hmm.. let's see what's in here.]===]";
local num, str = string.match(really_long_string, pattern);
print(really_long_string);
print(num, str);

print((string.gsub("hello Miko!", "%a", "%0+%0")));

-- interchange adjacent characters
print((string.gsub("hello Miko!", "(.)(.)", "%2%1")));

-- LaTex conversion
local latex = [[this is a \latex{command} and you need to 
\change{that}.]];
print((string.gsub(latex, "\\(%a+){(.-)}", "<%1>%2<%1>")));
