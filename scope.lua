local foo = foo; -- nil
io.write("foo: ", foo or "nil...", "\n");

bar = 2037;
local bar = bar;
io.write("bar: ", bar or "nil...", "\n");

do
    io.open = function (str) 
        print("io.open has changed its definition!", str); 
    end
end
io.open("modified io.open()?");
