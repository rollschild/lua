-- print the first non-empty input line
local line;
repeat
    line = io.read();
until line ~= "";
io.write(line);

-- print until an empty line is encountered
local another_line;
repeat
    another_line = io.read();
until another_line ~= "";
io.write(another_line);

