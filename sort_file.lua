local lines = {};

for line in io.lines() do
    lines[#lines + 1] = line;
end

table.sort(lines);

for _, line in ipairs(lines) do
    io.write(line, "\n");
end

