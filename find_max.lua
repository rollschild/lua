function max (arr)
    local index = 1;
    local value = arr[index];
    for pos = 2, #arr do
        if arr[pos] > value then
            index = pos;
            value = arr[pos];
        end
    end
    return value, index;
end

print(max({123456, 26, -1, 2037, 6}));
