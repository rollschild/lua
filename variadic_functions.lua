function sum_array (...)
    local sum = 0;
    for _, value in ipairs({...}) do
        sum = sum + value;
    end
    return sum;
end

print(sum_array(12, 6, -1, 0, 9)); -- 26
