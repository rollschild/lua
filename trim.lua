-- trim a string
function trim (str)
    return string.gsub(str, "^%s*(.-)%s*$", "%1");
end

str1 = "  this is a string with some spaces..       ";
print((trim(str1)));
