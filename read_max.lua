while true do
    local num1, num2, num3 = io.read("n", "n", "n");
    if not num1 then break; end
    io.write(string.format("largest number is %d.\n", 
        math.max(num1, num2, num3)));
end

