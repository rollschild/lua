ip_addresses = {
    {name = "Jovi", IP = "1.1.1.1"},
    {name = "Eva", IP = "12.3.101.249"},
    {name = "Wendy", IP = "127.0.0.1"},
    {name = "Susan", IP = "255.255.255.255"},
}

table.sort(ip_addresses, function (a, b) return a.name > b.name; end);

for _, pair in ipairs(ip_addresses) do
    io.write(string.format("name is: %s, and IP address is %s\n", pair.name, pair.IP));
end

