-- all words
local str = "this is a string.. yep";
local words = {};

for word in string.gmatch(str, "%a+") do
    words[#words + 1] = word;
end

for _, word in ipairs(words) do
    io.write(string.format("%s\n", word));
end
