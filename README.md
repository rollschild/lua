# Lua

Process and progress of learning Lua, mainly by reading the book _Programming in Lua, 4th Edition_.  
Concepts, practices, code snippets, and more.

## Notes:

- Always use io.write(); use print() ONLY for quick and dirty programs or debugging.
- io.write() adds NO extra chars (tabs/newlines, etc.); print() automatically tostring() its arguments.
- for full control, use string.format() in io.write().
- io.read(0) works as a test for end of file: it returns an empty string if there's more to be read or nil otherwise.
- three predefined handles:
  - io.stdin
  - io.stdout
  - io.stderr
- other operations on files:
  - io.tmpfile()
  - flush()
  - setvbuf()
  - f:seek(whence, offset)
  - os.rename(file)
  - os.remove(file)
- Other system calls
  - os.exit()
  - os.getenv("HOME")
- Use local variables wherever possible
- do-end
- `~=` is "Not equal to"
- in Lua the scope of a local variable declared inside the loop includes the condition
- ::label_name::
- the scope of a local variable ends on the last _non-void_ statement of the block wehre the variable is defined.
- Use `do end` to limit the scope of variables.
- `string.gsub("all lii", "l", "x", 2)` the fourth argument is to limit the number of substitutions to be made
- `string.gsub()` returns
  - a modified new string
  - a second result: number of times it made the substitution
- `print((string.gsub()))` uses an extra pair of parantheses to ensure only one output.
- Count the number of vowels in a text:
  - `_, num_vow = string.gsub(text, "[AEIOUaeiou]", "")`
- `"[+-]?%d+"` matches any number with or without sign
- `if string.find(str, "^[+-]?%d+$") then ...` an integer number
- `"%bxy"` for example: `"%b()"` - match substring starts with x and ends with y
- _frontier pattern_:
  - `%f[char-set]`
- use \ to escape `"`
- _captures_:
  - between paratheses
  - `%0` captures the whole match
- `_G` is a predefined table containing all global variables
- an empty capture like `()` captures its position in the subject string, as a number
  - `print(string.match("hello", "()ll()"))`
- Do NOT write a pattern that ends with the minus modifier `-`
  - it will only return the empty string
  - ALWAYS write something after it to anchor its expansion
- a long line (70 chars or more)
  - `pattern = string.rep("[^\n]", 70) .. "+"`
  -
