N = 8 -- define board size

function is_possible (board, row, col)
    for r = 1, row - 1, 1 do
        if (board[r] == col) or
           (board[r] - r == col - row) or
           (board[r] + r == col + row) then
            return false;
        end
    end
    return true;
end

function print_map (board)
    for row = 1, N do
        for col = 1, N do
            io.write(board[row] == col and "X" or "-", " ");
        end
        io.write("\n");
    end
    io.write("\n");
end

function add_queen (board, nth_queen)
    if nth_queen > N then
        print_map(board);
        return true;
    else
        for col = 1, N do
            if is_possible(board, nth_queen, col) then
                board[nth_queen] = col;
                if add_queen(board, nth_queen + 1) then
                    return true;
                end;
            end
        end
        return false;
    end
end

add_queen({}, 1);
