function add (...)
    local sum = 0;
    for i = 1, select("#", ...) do
        print("args: ", select(i, ...));
        sum = sum + select(i, ...); -- can only return one value
    end
    return sum;
end

print(add(12, -9, 2037, 121)); -- 2161
