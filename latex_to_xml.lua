-- able to handle nested commands
function latex_to_xml (ltx)
    local str = string.gsub(ltx, "\\(%a+)(%b{})", function (tag, body)
        body = string.sub(body, 2, -2); -- remove { and }
        body = latex_to_xml(body); -- recursion
        return string.format("<%s>%s<%s>", tag, body, tag);
    end
    );
    return str;
end

local latex = [[\nested{\latex{command}}]];
print(latex_to_xml(latex));

