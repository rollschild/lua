-- preprocess the subject string
local encode;
local decode;

function encode (str)
    return (string.gsub(str, "\\(.)", function (c)
        return string.format("\\%03d", string.byte(c));
    end));
end

-- print(encode([[I said, \"good..\"!]]));

function decode (str)
    return (string.gsub(str, "\\(%d%d%d)", function (num)
        return "\\" .. string.char(tonumber(num));
    end));
end

print(decode(string.gsub(encode([[I said,"\"This is good!\""]]), '".-"', string.upper)));
