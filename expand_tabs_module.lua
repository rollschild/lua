local expand_tabs_module = {}
local expand_tabs;
function expand_tabs_module.expand_tabs (str, tab)
    tab = tab or 4;
    local corr = 0;
    str = string.gsub(str, "()\\t", function (position)
            local num_of_spaces = tab - (position - 1 + corr) % tab;
            corr = corr + num_of_spaces - 1;
            return string.rep(" ", num_of_spaces);
        end);
    return str;
end

-- print(expand_tabs_module.expand_tabs("hello world..\t..."));
return expand_tabs_module;
