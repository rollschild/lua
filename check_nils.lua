function no_holes(...)
    local args = table.pack(...); -- extra field *n*
    for i = 1, args.n do
        if args[i] == nil then
            return false;
        end
    end
    return true;
end

print("No nils in args? ", no_holes(12, "no nils", -6));
