function derivative (func, delta)
    delta = delta or 1e-4;
    return function (x)
        return (func(x + delta) - func(x)) / delta;
    end
end

res = derivative(math.cos);
print(-res(5.2), math.sin(5.2));
