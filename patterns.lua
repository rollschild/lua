local code_snippet = "int x; /* comment */ int y; /* another comment...*/";
print((string.gsub(code_snippet, "/%*.*%*/", "")));

-- lazy matching
print((string.gsub(code_snippet, "/%*.-%*/", "")));
