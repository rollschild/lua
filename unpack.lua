-- table.unpack implemented in Lua
function unpack (tb, i, n)
    i = i or 1;
    n = n or #tb;
    if i <= n then
        return tb[i], unpack(tb, i + 1, n); -- multiple returns
    end
end

print(unpack({14, 5, 6, 9})); -- 14 5 6 9
