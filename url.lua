local unescape;
function unescape (str)
    local res = string.gsub(str, "+", " ");
    res = string.gsub(res, "%%(%x%x)", function (param)
        return string.char(tonumber(param, 16));
    end
    );
    return res;
end

print(unescape("a%2Bb+%3D+c"));

local cgi = {};
local decode;
function decode (query)
    for name, value in string.gmatch(query, "([^=&]+)=([^&=]+)") do
        name = unescape(name);
        value = unescape(value);
        cgi[name] = value;
    end
end
decode("name=al&query=a%2Bb+%3D+c");
for name, value in ipairs(cgi) do
    io.write(string.format("%s %s", name, value));
end

