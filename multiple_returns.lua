function foo ()
    return "a", "b";
end

print(foo(), 7); -- a 7

tb = {foo()};
print(#tb); -- 2

function foo_bar ()
    return foo();
end

print(foo_bar()); -- a b

function foo_bar_para ()
    return (foo());
end

print(foo_bar_para()); -- a
