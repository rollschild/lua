function foo (x, y)
    return "Be aware, ", "function foo() is called with args: ", x, y;
end

function trace (...)
    print('calling foo: ', ...);
    return foo(...);
end

print(trace(6, "this is the second arg"));
