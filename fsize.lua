local f = assert(io.open("./README.md", "r"));

function fsize (file)
    local current = file:seek();
    local size = file:seek("end"); -- set position to end
    file:seek("set", current);
    return size;
end

io.write(fsize(f));

io.close(f);
