-- computes the square root of a number
-- Newton-Raphson
local num = io.read("n");
local sqrt = num / 2;

repeat
    sqrt = (sqrt + num / sqrt) / 2;
    local error = math.abs(sqrt ^ 2 - num);
until error < num / 1000

io.write(string.format("approx. sqrt is: %f\n", sqrt));
